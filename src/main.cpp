#include <omp.h>
#include <iostream>
#include <iomanip>
#include "CStopWatch.h"
#include <vector>
#include <unistd.h>
#include <random>
#include <algorithm>

std::random_device rd;                                // only used once to initialise (seed) engine
std::mt19937 rng(rd());                               // random-number engine used (Mersenne-Twister in this case)
std::uniform_real_distribution<double> myDist{-1, 1}; // guaranteed unbiased

double firstPiMethod(int n)
{
    double sum = 0.0;
    double factor = 1.0;

    for (int i = 0; i < n; i++, factor = -factor)
    {
        sum += factor / (2 * i + 1);
    }
    return 4.0 * sum;
}

double secondPiMethod(int nSamples)
{

    double x, y;
    double estimate = 0.0;
    int numInCircle = 0.00;

    for (int i = 0; i < nSamples; i++)
    {
        x = ((double)std::rand() / (double)RAND_MAX) * 2.0 - 1.0;
        y = ((double)std::rand() / (double)RAND_MAX) * 2.0 - 1.0;

        if (x * x + y * y <= 1)
        {
            numInCircle++;
        }
    }
    estimate = (4.0 * numInCircle / ((double)nSamples));
    return estimate;
}

void usingSections()
{
    int threadMin, threadMax, threadStep;
    int numTrials, n;
    CStopWatch timer;
    double result1, result2;
    double time1, time2;

    threadMin = 1;
    threadMax = 10;
    threadStep = 1;
    numTrials = 1;
    n = 500000000;

    for (int numThreads = threadMin; numThreads <= threadMax; numThreads += threadStep)
    {
        for (int curTrial = 0; curTrial < numTrials; curTrial++)
        {

            omp_set_num_threads(numThreads);

            #pragma omp parallel sections num_threads(numThreads)
            {
                #pragma omp section
                {
                    // Calling first Pi Method
                    timer.startTimer();
                    result1 = firstPiMethod(n);
                    timer.stopTimer();
                    time1 = timer.getElapsedTime();
                }
                #pragma omp section
                {
                    // Calling second Pi Method
                    timer.startTimer();
                    result2 = secondPiMethod(n);
                    timer.stopTimer();
                    time2 = timer.getElapsedTime();
                }
            }
   
            // Output
            std::cout << std::setw(2) << numThreads << " ";
            std::cout << std::setprecision(20) << std::fixed << result1 << " ";
            std::cout << std::setprecision(5) << std::fixed << time1 << " ";
            std::cout << std::setprecision(20) << std::fixed << result2 << " ";
            std::cout << std::setprecision(5) << std::fixed << time2 << " ";
            std::cout << std::setprecision(5) << std::fixed << time1 + time2 << " ";
            std::cout << "\n";
        }
    }
}

void usingTasks()
{
    int threadMin, threadMax, threadStep;
    int numTrials, n;
    CStopWatch timer;
    double result1, result2;
    double time1, time2;

    threadMin = 1;
    threadMax = 10;
    threadStep = 1;
    numTrials = 1;
    n = 500000000;

    for (int numThreads = threadMin; numThreads <= threadMax; numThreads += threadStep)
    {
        for (int curTrial = 0; curTrial < numTrials; curTrial++)
        {

            omp_set_num_threads(numThreads);

            #pragma omp parallel sections num_threads(numThreads)
            {
                #pragma omp section
                {
                    // Calling first Pi Method
                    timer.startTimer();
                    result1 = firstPiMethod(n);
                    timer.stopTimer();
                    time1 = timer.getElapsedTime();
                }
                #pragma omp section
                {
                    // Calling second Pi Method
                    timer.startTimer();
                    result2 = secondPiMethod(n);
                    timer.stopTimer();
                    time2 = timer.getElapsedTime();
                }
            }

            // Output
            std::cout << std::setw(2) << numThreads << " ";
            std::cout << std::setprecision(20) << std::fixed << result1 << " ";
            std::cout << std::setprecision(5) << std::fixed << time1 << " ";
            std::cout << std::setprecision(20) << std::fixed << result2 << " ";
            std::cout << std::setprecision(5) << std::fixed << time2 << " ";
            std::cout << std::setprecision(5) << std::fixed << time1 + time2 << " ";
            std::cout << "\n";
        }
    }
}

int main()
{

    std::cout << "Using Sections\n";
    usingSections();

    std::cout << "Using Tasks\n";
    usingTasks();

    std::cout << "\n\n";

    omp_set_nested(true);

    std::cout << "Using Sections w/ Nesting\n";
    usingSections();

    std::cout << "Using Tasks w/ Nesting\n";
    usingTasks();

    return 0;
}
